# kustomize

## Installation

To install this project, copy all files and run:

```
# create the expenses namespace
kubectl create namespace expenses
# optional: set the curren default namespace to "expenses" for easier work with kubectl without "-n"
kubectl config set-context --current --namespace expenses
# apply the application into the new namespace and prune outdated objects
kubectl apply -k /path/to/your/directory --prune -l app.kubernetes.io/part-of=expenses -n expenses # ignore deprecation warning
# wait for all pods to be available
kubectl wait pods -n expenses -l app.kubernetes.io/part-of=expenses --for condition=Ready --timeout=180s
# now open your browser on http://localhost:9090
```

## License

This Project is licensed under MIT.
